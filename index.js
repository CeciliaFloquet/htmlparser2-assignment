
const axios = require('axios');
const htmlparser2 = require("htmlparser2");

let city='';
let temperature='';
let inCity = false;
let inTemperature= false;
let inDate = false;
let hi= true;
let countRowsCity = false;
let countRowsDate = false;
let countRowsTemperature=false;

let temp=1;
let test=5;

const parser = new htmlparser2.Parser(
    {
        onopentag(name, attribs) {
            if (name === "h1") {
                inCity = true;
            }
            if ( name === "dd"  && attribs ) {
                inDate= true;

            }
            if(name === "span"){
                inTemperature=true;
            }
        },
         onattribute(name, value) {
            if ( name === "id" && value === "wb-cont"){
                countRowsCity = true;
            }
            if ( name === "dd"  && value === "mrgn-bttm-0") {
                console.log("testpoha2");
                countRowsDate= true;
            }
            if ( name === "span"  && value === "wxo-metric-hide") {
                console.log("testpoha");
                countRowsTemperature= true;
            }
        },
        ontext(text) {
            if (inCity) {
                
                city= text;
                    console.log( city );
            }
            inCity = false;
            if(inDate && test> 0){
                if(text.match(/^[0-9][0-9]\W[0-9][0-9].*$/) || text.match(/^[0-9]\W[0-9][0-9]\W.*$/) ){
                   
                    console.log(text);  
                }
                
                test--;
            }
            inDate=false;
            
            if(inTemperature){
               temperature= text.substr(0,1);
               if(temperature.match(/^[0-9].*$/) && hi){
                    console.log(text);
                    hi= false;
               }
            }
            inTemperature=false;
        },
        onclosetag(tagname) {
            if (tagname === "html") {
                console.log("That's it!");
            }
            // if ( tagname === "table" && countRows && !done ) {
            //     console.log( "There are " + (rowCount-1) + " computer labs.");
            //     done = true;
            // }
            // if(c==3 && count){
            //     console.log("Test");
            // }
        }
    },
    { decodeEntities: true }
);


if ( process.argv.length > 2 ) {
    axios.get( process.argv[2] )
        .then( response => {
            parser.write( response.data );
            parser.end();
        })
        .catch( error => {
            console.log("Could not fetch page.");
        });
} else {
    console.log( "Missing URL argument" );
}